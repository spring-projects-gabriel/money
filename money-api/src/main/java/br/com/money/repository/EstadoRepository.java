package br.com.money.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.money.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long> {

}
