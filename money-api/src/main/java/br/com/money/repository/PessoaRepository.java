package br.com.money.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.money.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

	Page<Pessoa> findByNome(String nome, Pageable pageable);

	//Page<Pessoa> filtrar(PessoaFilter pessoaFilter, Pageable pageable);

}
