package br.com.money.repository.lancamento;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.money.model.Lancamento;
import br.com.money.model.dto.LancamentoEstatisticaCategoria;
import br.com.money.model.dto.LancamentoEstatisticaDia;
import br.com.money.model.dto.LancamentoEstatisticaPessoa;
import br.com.money.model.dto.ResumoLancamento;
import br.com.money.model.form.LancamentoForm;

public interface LancamentoRepositoryQuery {

	public List<LancamentoEstatisticaPessoa> porPessoa(LocalDate inicio, LocalDate fim);

	public List<LancamentoEstatisticaCategoria> porCategoria(LocalDate mesReferencia);

	public List<LancamentoEstatisticaDia> porDia(LocalDate mesReferencia);

	public Page<Lancamento> filtrar(LancamentoForm lancamentoForm, Pageable pageable);

	public Page<ResumoLancamento> resumir(LancamentoForm lancamentoForm, Pageable pageable);

}
