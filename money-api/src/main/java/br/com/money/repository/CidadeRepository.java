package br.com.money.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.money.model.Cidade;

public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	List<Cidade> findByEstadoId(Long idEstado);

}
