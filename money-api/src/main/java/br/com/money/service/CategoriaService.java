package br.com.money.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.money.model.Categoria;
import br.com.money.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;

	public Page<Categoria> filtrar(String nome, Pageable pageable) {
		return categoriaRepository.findByNome(nome, pageable);
	}

	public Optional<Categoria> buscarPeloNome(String nome) {
		return categoriaRepository.findByNome(nome);
	}

	public Categoria atualizar(Long id, Categoria categoria) {
		Categoria categoriaSalva = buscarPeloId(id);
		BeanUtils.copyProperties(categoria, categoriaSalva, "id"); // Realizo update, passo a categoria nova, a categoria existente e os atributos que serão ignorados   
		
		return categoriaRepository.save(categoriaSalva);
	}

	public Categoria atualizarNome(Long id, String nome) {
		Categoria categoriaSalva = buscarPeloId(id);
		
		categoriaSalva.setNome(nome);
		
		return categoriaRepository.save(categoriaSalva);
	}
	
	public Categoria buscarPeloId(Long id) {
		Optional<Categoria> categoriaSalva = categoriaRepository.findById(id);

		if (!categoriaSalva.isPresent()) {
			throw new EmptyResultDataAccessException(1); // Espero pelo menos um resultado
		}

		return categoriaSalva.get();
	}


}
