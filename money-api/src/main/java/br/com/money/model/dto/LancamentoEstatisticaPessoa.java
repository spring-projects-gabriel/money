package br.com.money.model.dto;

import java.math.BigDecimal;

import br.com.money.model.Pessoa;
import br.com.money.model.TipoLancamento;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LancamentoEstatisticaPessoa {

	private TipoLancamento tipo;
	private Pessoa pessoa;
	private BigDecimal total;

}
