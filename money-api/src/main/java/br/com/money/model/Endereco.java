package br.com.money.model;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Embeddable
public class Endereco {

	private String logradouro;

	private String numero;

	private String complemento;

	private String bairro;

	private String cep;

	@ManyToOne
	@JoinColumn(name = "codigo_cidade")
	private Cidade cidade;

}
