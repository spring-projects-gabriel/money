package br.com.money.model.abs;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import br.com.money.model.Estado;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Estado.class)
public abstract class Estado_ {

	public static volatile SingularAttribute<Estado, Long> codigo;
	public static volatile SingularAttribute<Estado, String> nome;

	public static final String CODIGO = "codigo";
	public static final String NOME = "nome";

}
