package br.com.money.model.form;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PessoaForm {

	private String nome;

}
