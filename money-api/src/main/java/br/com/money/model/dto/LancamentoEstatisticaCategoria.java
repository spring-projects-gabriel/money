package br.com.money.model.dto;

import java.math.BigDecimal;

import br.com.money.model.Categoria;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LancamentoEstatisticaCategoria {

	private Categoria categoria;

	private BigDecimal total;

}
