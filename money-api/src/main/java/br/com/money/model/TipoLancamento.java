package br.com.money.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoLancamento {

	RECEITA("Receita"), DESPESA("Despesa");

	private String descricao;

}
