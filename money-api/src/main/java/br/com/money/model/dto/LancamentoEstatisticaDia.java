package br.com.money.model.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.money.model.TipoLancamento;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LancamentoEstatisticaDia {

	private TipoLancamento tipo;
	private LocalDate dia;
	private BigDecimal total;

}
