package br.com.money.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.money.model.Cidade;
import br.com.money.repository.CidadeRepository;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/cidades")
public class CidadeResource {

	@Autowired
	private CidadeRepository cidadeRepository;

	@ApiOperation(value = "Retornar lista de cidades pelo id do estado")
	@GetMapping
	public List<Cidade> pesquisar(@RequestParam Long idEstado) {
		return cidadeRepository.findByEstadoId(idEstado);
	}

}
