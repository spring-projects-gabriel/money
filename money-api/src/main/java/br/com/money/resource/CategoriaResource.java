package br.com.money.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.money.model.Categoria;
import br.com.money.repository.CategoriaRepository;
import br.com.money.service.CategoriaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
	private CategoriaService categoriaService;

	@ApiOperation(value = "Retornar lista de categorias pelo nome")
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Retorna a lista de categoria"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
		    @ApiResponse(code = 404, message = "Recurso não encontrado"),
		    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@GetMapping
	public Page<Categoria> pesquisar(@RequestParam String nome, Pageable pageable) {
		return categoriaService.filtrar(nome, pageable);
	}

	@ApiOperation(value = "Salvar nova categoria")
	@PostMapping
	public ResponseEntity<Categoria> criar(@Valid @RequestBody Categoria categoria) {
		Categoria categoriaSalva = categoriaRepository.save(categoria);

		return ResponseEntity.status(HttpStatus.CREATED).body(categoriaSalva);
	}

	@ApiOperation(value = "Buscar categoria pelo id")
	@GetMapping("/{id}")
	public ResponseEntity<Categoria> buscar(@PathVariable Long id) {
		Optional<Categoria> categoria = categoriaRepository.findById(id);

		return categoria.isPresent() ? ResponseEntity.ok(categoria.get()) : ResponseEntity.notFound().build();
	}

	@ApiOperation(value = "Remover categoria pelo id")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable Long id) {
		return categoriaRepository.findById(id).isPresent() ? ResponseEntity.ok("Categoria de id " + id + "excluída com sucesso!") : ResponseEntity.notFound().build();
	}
	
	@ApiOperation(value = "Atualizar categoria pelo id")
	@PutMapping("/{id}")
	public ResponseEntity<Categoria> atualizar(@PathVariable Long id, @Valid @RequestBody Categoria categoria){
		Categoria categoriaSalva = categoriaService.atualizar(id, categoria);
		
		return ResponseEntity.ok(categoriaSalva);
	}
	
	@ApiOperation(value = "Atualizar nome da categoria")
	@PutMapping("/{id}/nome")
	@ResponseStatus(HttpStatus.NO_CONTENT) // Retorno status 204 quando tudo der certo
	public void atualizarNome(@PathVariable Long id, @RequestBody String nome) {
		categoriaService.atualizarNome(id, nome);
	}
	
}
