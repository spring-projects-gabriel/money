# Curso Algaworks fullstack Angular e Spring

## Conteúdo back-end
# 1. Introdução ao REST

1.1. Introdução ao curso

1.2. Como usar o suporte

1.3. O que é SOFEA?

1.4. O que é REST?

1.5. Conhecendo o projeto do curso

1.6. Ambiente de desenvolvimento REST

1.7. Testando APIs com Postman

1.8. Introdução ao protocolo HTTP


# 2. Fundamentos do REST
2.1. O que é um recurso?

2.2. Representações de um recurso

2.3. Modelo de maturidade Richardson - Nível 0

2.4. Modelo de maturidade Richardson - Nível 1

2.5. Modelo de maturidade Richardson - Nível 2

2.6. Modelo de maturidade Richardson - Nível 3

2.7. HATEOAS

2.8. Segurança de APIs REST

2.9. Idempotência


# 3. Primeiras consultas e cadastros na API
3.1. Criando o projeto da API

3.2. Conectando ao MySQL

3.3. Migração de dados com Flyway

3.4. Consultando primeiro recurso com GET

3.5. Coleção vazia, o que retornar?

3.6. Cadastrando nova categoria com POST

3.7. Desafio: Retornar 404 caso não exista a categoria

3.8. Validando atributos desconhecidos

3.9. Tratando erros com ExceptionHandler

3.10. Validando valores inválidos com Bean Validation

3.11. Desafio: Criando o cadastro de pessoa

3.12. Usando eventos para adicionar header Location


# 4. Atualização e remoção de recursos na API
4.1. Removendo pessoa com DELETE

4.2. Sobre atualização de recursos REST

4.3. Atualizando pessoa com PUT

4.4. Implementando atualização parcial com PUT


# 5. Relacionamentos entre recursos REST
5.1. Criando a migração e entidade de lançamento

5.2. Desafio: Lista e busca de lançamentos

5.3. Desafio: Cadastrando o primeiro lançamento

5.4. Validando inconsistências

5.5. Validando lançamento com Bean Validation

5.6. Regra para não salvar pessoa inativa

5.7. Implementando pesquisa de lançamento com Metamodel

5.8. Desafio: Removendo lançamentos

5.9. Implementando a paginação de lançamentos


# 6. Segurança da API
6.1. Implementando autenticação Basic

6.2. Fluxo básico do OAuth

6.3. Implementando segurança com OAuth 2 e Password Flow

6.4. JSON Web Tokens - JWT

6.5. Configurando JWT no projeto

6.6. Renovando o access token com o refresh token

6.7. Movendo o refresh token para o cookie

6.8. Movendo o refresh token do cookie para a requisição

6.9. O que é CORS?

6.10. Criando filtro para CORS

6.11. Movendo o usuário para o banco de dados

6.12. Adicionando permissões de acesso

6.13. Desafio: Finalizando permissões de acesso

6.14. Implementando o logout

# 7. Deploy da API em produção
7.1. Implementando projeção de lançamento

7.2. Profiles do Spring

7.3. Criando a conta no Heroku

7.4. Deploy da API na nuvem

7.5. Nome do usuário no token JWT

7.6. Alternando OAuth 2 e Basic Security com profiles

7.7. Desafio: Pesquisa de pessoa

7.8. Ajustando o CEP

7.9. Desafio: Atualização de lançamento